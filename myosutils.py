#!/usr/bin/env python
#
# Copyright (c) 2017 Shaheen H. Ali
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

"""
A collection of utilities
"""
import os

def getType(fname):
    """
    Determine the music file type
    """
    comps = fname.split('.')
    return comps[-1]

def changeSuffix(fname, suffix):
    """
    Replace the suffix of a specific filename
    """
    if suffix[0] != '.':
        mysuffix = '.' + suffix
    else:
        mysuffix = suffix
        
    return os.path.splitext(fname)[0] + mysuffix

if __name__ == '__main__':
    import sys

    '''Self tests:
    Print suffix
    Change suffix'''
    
    if len(sys.argv) < 2:
        print 'Argument required'
        sys.exit(1)

    fname = sys.argv[1]
    weirdfname = 'hello.forth.theseventh.mp3'
    print 'The suffix of "' + fname + '" is "' + getType(fname) + '"'
    print 'The suffix of wierd file "' + weirdfname + '" is "' + getType(weirdfname) + '"'
    print 'Change suffix to "mp3" results in "' + changeSuffix(fname, 'mp3') + '"'
    print 'Change suffix to "doc" results in "' + changeSuffix(fname, '.doc') + '"'
    print 'Change suffix to wierd ",idx" results in "' + changeSuffix(fname, ',idx') +  '"'
    print 'Change suffix of wierd file "' + weirdfname + '" to org results in "' + changeSuffix(weirdfname, 'org')  + '"'
    
    sys.exit(0)
