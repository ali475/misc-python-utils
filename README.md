# Miscellaneous Python Utilities

A found collection of python recipes pieced together in a way that helps me understand how to do

* Options processing
* OS suffix handling

## Options processing

* prog1arg: A python program that takes non-optional argument
* prog1opt1arg: A python program that takes one non-optional argument and one option

## OS suffix handling

* myosutils.py: Replace a filename suffix
